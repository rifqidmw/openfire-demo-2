package com.kpblog.textchat;

/**
 * Created by Khushvinders on 15-Nov-16.
 */

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kpblog.textchat.utils.Api;
import com.kpblog.textchat.utils.Handle;
import com.kpblog.textchat.utils.ParamReq;

import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;


public class UserList extends Fragment implements OnClickListener {

    List<User> users = new ArrayList<User>();
    ListView userList;
    private Callback<ResponseBody> cBack;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.userlist_layout, container, false);
        userList = (ListView)view.findViewById(R.id.userList);
        getUsrList();
        return view;
    }

    RequestQueue requestQueue;
    UserListAdapter adapter;
    public void getUsrList(){
        Call<ResponseBody> call = ParamReq.requestUserAll(getActivity());
        cBack = new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());


                    JSONArray data = jsonObject.getJSONArray("users");
                    if (data.length() >= 0) {
                        Log.d("data123", Integer.toString(data.length()));
                        for (int i = 0; i < data.length(); i++) {
                            User user = new User();
                            user.setName(data.getJSONObject(i).getString("name"));
                            user.setUsername(data.getJSONObject(i).getString("username"));
                            user.setEmail(data.getJSONObject(i).getString("email"));
                            Log.d("name123", data.getJSONObject(i).getString("name"));
                            Api.userResponses.add(user);
                        }
                    } else {
                        Log.d("user", "data not found");
                    }

                    adapter = new UserListAdapter(getActivity(), Api.userResponses);
                    userList.setAdapter(adapter);
                    boolean handle = Handle.handleUserAll(response.body().string(), getActivity());
                    if (handle) {
                        Log.d("response retrofit", response.body().string());

                    } else {

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Api.retryDialog(getActivity(), call, cBack, 1, false);
            }
        };
        Api.enqueueWithRetry(getActivity(), call, cBack, false, "Loading");
//        requestQueue = Volley.newRequestQueue(getActivity());
//        try {
//            StringRequest strReq = new StringRequest(Request.Method.GET,"http://172.27.13.29:9090/plugins/restapi/v1/users", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("response", response);
//              //  System.out.println("Response : "+response);
//                UserResponse userResponse = new Gson().fromJson(response, UserResponse.class);
////                users.clear();
//                users.addAll(Arrays.asList(userResponse.getUser()));
//                adapter = new UserListAdapter(getActivity(), users);
//                userList.setAdapter(adapter);
//            }
//        }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Log.d("volleyError", error.toString());
//                        error.printStackTrace();
//                    }
//                }){
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Authorization", "717mL9U8xwZJTLze");
//                headers.put("Accept", "application/json");
//                return headers;
//            }
//        };
//        requestQueue.add(strReq);
//    }
//    catch (Exception e){
//        e.printStackTrace();
//    }
}

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

}